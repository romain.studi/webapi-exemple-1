﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using MySql.Data.MySqlClient;
using Microsoft.AspNetCore.Cors;

namespace webapi_exemple_1.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [EnableCors("AllowMyOrigin")]
    public class WeatherForecastController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private readonly ILogger<WeatherForecastController> _logger;

        private readonly IConfiguration _configuration;

        public WeatherForecastController(ILogger<WeatherForecastController> logger, IConfiguration configuration)
        {
            _logger = logger;
            _configuration = configuration;
        }

        [HttpGet]
        [EnableCors("AllowMyOrigin")]
        public async Task<IEnumerable<WeatherForecast>> GetAsync()
        {
            var rng = new Random();

            IEnumerable <WeatherForecast> generatedWeather = Enumerable.Range(1, 4).Select(index => new WeatherForecast
            {
                Date = DateTime.Now.AddDays(index),
                TemperatureC = rng.Next(-20, 55),
                Summary = Summaries[rng.Next(Summaries.Length)]
            })
            .ToArray();

            using (IDbConnection conn = new MySqlConnection(_configuration.GetConnectionString("WeatherDb")))
            {
                string sQuery = @"
                    INSERT INTO weather (weather_date,temperature,summary)
                    VALUES (@WEATHERDATE,@TEMPERATURE,@SUMMARY);";

                conn.Open();

                await conn.ExecuteAsync(sQuery, new
                {
                    WEATHERDATE = generatedWeather.First().Date,
                    TEMPERATURE = generatedWeather.First().TemperatureC,
                    SUMMARY = generatedWeather.First().Summary
                });
            }

            return generatedWeather;
        }
    }
}
